# t_manager

### Overview 

There are three component to this App 

- Backend (Django: TCP/Websockets & REST Http Server)
- Database (MySQL)
- Websocket Demo App (Vue JS)




Setup Instruction 

- `docker compose up --build`

Run Database migration 
- `docker-compose exec backend python manage.py migrate`

To run automated tests 
- `docker-compose exec backend python manage.py test`

Create superuser incase you will like to use the admin page and follow the prompt
- `docker-compose exec backend python manage.py createsuperuser` 

If you prefer to see messages sent via websockets on the terminal 
- `npm install -g wscat`
-  `wscat -c ws://127.0.0.1:8888/ws/`

URLS

[Backend: http://localhost:8888/api](http://localhost:8888/api)


[Websocket Client: http://localhost:8080](http://localhost:8080)

[Postman Collection](https://www.postman.com/grey-capsule-574547/workspace/t-manager/environment/10026788-9d194fc0-7385-47a3-a623-4b8d18f5d7f2?action=share&creator=10026788&active-environment=10026788-9d194fc0-7385-47a3-a623-4b8d18f5d7f2)

[Admin Dashboard: http://localhost:8888/admin](http://localhost:8888/admin)



### Expected result 
You can either add task through the admin dasboard via the browser or use postman. Whilst you add these new tasks, they should show up on the websocket client app demo (see link to app above)


<div style="text-align: center;">
    <img src="tasks.png" width="400" height="350">
</div>