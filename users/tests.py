import factory
from django.contrib.auth.models import User
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from django.contrib.auth.models import User
from rest_framework_simplejwt.tokens import RefreshToken


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = User

    username = factory.Faker('user_name')
    email = factory.Faker('email')
    password = factory.PostGenerationMethodCall('set_password', 'secure_pass123')
    
    


class UserRegistrationTests(APITestCase):
    def test_register_user(self):
        url = reverse('user_registration')
        data = {
            'username': 'testuser',
            'password': 'testpassword',
            'email': 'testuser@example.com'
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(User.objects.get().username, 'testuser')

class UserRoleUpdateTests(APITestCase):
    def setUp(self):
        self.admin_user = UserFactory(is_staff=True, is_superuser=True)
        self.regular_user = UserFactory()
        self.token = RefreshToken.for_user(self.admin_user).access_token

    def test_admin_can_update_user_role(self):
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.token}')
        url = reverse('user_role_update', kwargs={'pk': self.regular_user.id})
        data = {
            'is_staff': True,
            'is_superuser': False
        }
        response = self.client.patch(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.regular_user.refresh_from_db()
        self.assertTrue(self.regular_user.is_staff)
        self.assertFalse(self.regular_user.is_superuser)

    def test_non_admin_cannot_update_user_role(self):
        token = RefreshToken.for_user(self.regular_user).access_token
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {token}')
        url = reverse('user_role_update', kwargs={'pk': self.admin_user.id})
        data = {
            'is_staff': False,
            'is_superuser': False
        }
        response = self.client.patch(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
