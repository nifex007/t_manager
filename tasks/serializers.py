# serializers.py

from rest_framework import serializers
from django.contrib.auth.models import User
from .models import Task

class TaskSerializer(serializers.ModelSerializer):
    owner = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False)

    class Meta:
        model = Task
        fields = ['id', 'title', 'description', 'created_at', 'updated_at', 'due_date', 'priority', 'status', 'owner']

    def update(self, instance, validated_data):
        if 'owner' in validated_data and not self.context['request'].user.is_staff:
            raise serializers.ValidationError("Only admin users can update the owner field.")
        return super().update(instance, validated_data)
