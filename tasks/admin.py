from django.contrib import admin
from .models import Task

class TaskAdmin(admin.ModelAdmin):
    list_display = ('title', 'owner', 'due_date', 'status', 'priority')
    list_filter = ('priority', 'status', 'due_date', 'owner')
    search_fields = ('title', 'description')

admin.site.register(Task, TaskAdmin)