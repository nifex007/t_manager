import factory
from tasks.models import Task
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework_simplejwt.tokens import RefreshToken
from tasks.models import Task
from users.tests import UserFactory


class TaskFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Task

    title = factory.Faker('sentence')
    description = factory.Faker('paragraph')
    created_at = factory.Faker('date_time_this_year')
    updated_at = factory.Faker('date_time_this_year')
    due_date = factory.Faker('future_datetime')
    priority = 'medium'
    status = 'todo'
    owner = factory.SubFactory(UserFactory)



class TaskViewSetTests(APITestCase):

    def setUp(self):
        self.user = UserFactory()
        self.admin_user = UserFactory(is_staff=True, is_superuser=True)
        self.task = TaskFactory(owner=self.user)
        self.token = RefreshToken.for_user(self.user).access_token
        self.admin_token = RefreshToken.for_user(self.admin_user).access_token

    def test_create_task(self):
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.token}')
        url = reverse('task-list')
        data = {
            'title': 'New Task',
            'description': 'Task description',
            'due_date': '2024-12-31T23:59:59Z',
            'priority': 'medium',
            'status': 'todo'
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Task.objects.count(), 2)
        self.assertEqual(Task.objects.latest('id').title, 'New Task')

    def test_list_tasks(self):
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.token}')
        url = reverse('task-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.data[0]['title'], self.task.title)

    def test_retrieve_task(self):
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.token}')
        url = reverse('task-detail', kwargs={'pk': self.task.id})
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['title'], self.task.title)

    def test_update_task(self):
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.token}')
        url = reverse('task-detail', kwargs={'pk': self.task.id})
        data = {'title': 'Updated Task'}
        response = self.client.patch(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.task.refresh_from_db()
        self.assertEqual(self.task.title, 'Updated Task')

    def test_delete_task_by_owner(self):
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.token}')
        url = reverse('task-detail', kwargs={'pk': self.task.id})
        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Task.objects.count(), 0)

    def test_delete_task_by_admin(self):
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.admin_token}')
        url = reverse('task-detail', kwargs={'pk': self.task.id})
        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Task.objects.count(), 0)

    def test_cannot_see_task_by_non_owner(self):
        another_user = UserFactory()
        another_token = RefreshToken.for_user(another_user).access_token
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {another_token}')
        url = reverse('task-detail', kwargs={'pk': self.task.id})
        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(Task.objects.count(), 1)

    def test_admin_can_update_owner(self):
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.admin_token}')
        url = reverse('task-detail', kwargs={'pk': self.task.id})
        new_owner = UserFactory()
        data = {'owner': new_owner.id}
        response = self.client.patch(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.task.refresh_from_db()
        self.assertEqual(self.task.owner.id, new_owner.id)
