from channels.db import database_sync_to_async
from djangochannelsrestframework import permissions
from djangochannelsrestframework.generics import GenericAsyncAPIConsumer
from djangochannelsrestframework.mixins import ListModelMixin
from djangochannelsrestframework.observer import model_observer
import json

from .models import Task
from .serializers import TaskSerializer


class TaskConsumer(ListModelMixin, GenericAsyncAPIConsumer):
    queryset = Task.objects.all()
    serializer_class = TaskSerializer
    permissions = (permissions.AllowAny,)

    async def connect(self, **kwargs):
        await self.accept()
        await self.model_change.subscribe()
        await self.send_task_list()
        
    async def send_task_list(self):
        tasks = await database_sync_to_async(list)(self.queryset)
        await self.send_json({
            'action': 'list',
            'data': TaskSerializer(tasks, many=True).data
        })

    @model_observer(Task)
    async def model_change(self, message, observer=None, **kwargs):
        await self.send_json(message)

    @model_change.serializer
    def model_serialize(self, instance, action, **kwargs):
        return dict(data=TaskSerializer(instance=instance).data, action=action.value)
    
    
async def receive_json(self, content, **kwargs):
    action = content.get('action')

    if action == 'list':
        await self.send_task_list()




