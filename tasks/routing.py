# from django.urls import url
# from . import consumers


# websocket_urlpatterns = [
#     url(r'^ws/tasks/$', consumers.TaskConsumer.as_asgi()),
#     ]


from django.urls import re_path

from . import consumers


websocket_urlpatterns = [re_path(r"^ws/$", consumers.TaskConsumer.as_asgi())]