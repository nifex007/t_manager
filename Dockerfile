
FROM python:3.8

WORKDIR /code

ADD . /code

RUN pip install --upgrade pip
RUN pip install -r requirements.txt

EXPOSE 8888

ENV PYTHONUNBUFFERED=1

CMD ["python",  "manage.py", "migrate"]
